# Unsupervized Image Clusterization

A simple and efficient way to explore a large quantity of images

## Requirements

For this algorithm, I used the following python libraries:

- `keras==2.2.4`  
- `pandas==0.24.1`  
- `scikit-learn==0.22.2.post1`  
- `matplotlib==3.0.3`  
- `numpy==1.18.2`

